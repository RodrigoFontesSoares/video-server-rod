# Abstract

 ARMIS Intelligent Transport Systems' (ITS) main solution is "**DRIVE**", a highway traffic and infrastructure management platform made available to highway concession holders (i.e. companies responsible for operating highways in Portugal on behalf of the State). DRIVE aggregates several sub-modules responsible for different business concerns. Among other features, DRIVE is capable of managing telemetric equipments, such as road-side **Cameras**. As of this writing, ARMIS ITS is planning to integrate (TODO is it already integrated, at least partially? I'm not sure if the Video Server is already "live" or not) a "**Video Server**" component into DRIVE, capable of interfacing with Cameras and allowing operators to monitor their feeds. To enhance this Video Server's value-offer to ARMIS ITS' clients, a **Video Analytics** component is to be added to the Video Server. This **internship's purpose** is to integrate said Video Analytics into the Video Server and, after that, integrate the Video Server into the main DRIVE solution.    

# Video Server and the Video Analytics component

 As of this moment, the Video Analytics component is undergoing development as a separate program written in C++, already possessing some analytic capabilities such as detecting if a vehicle is driving against the direction of traffic, if it is stopped on a highway, among others.

 The Video Server is currently implemented in Java/Vert.x, and a separate proof-of-concept Vert.x program ("Prototype") capable of calling the Video Analytics C++ program through a HTTP request has been developed. Therefore, the goal is to integrate this preliminary "Prototype" into the Video Server, in a way that DRIVE can request different Analytic features from the Video Server through corresponding HTTP requests.

# Requirements Analysis


 This analysis is based on the FURPS+ model - **F**unctionality, **U**sability, **R**eliability, **P**erformance, **S**upportability and more (**+**). Functionality comprises all functional requirements in the form of Use Cases (UC). All other requirement types (URPS+) are under the "Non-Functional Requirements" section


## Functional Requirements (F)

### Key Domain Concepts

#### Analytic

An Analytic is capable of detecting a specific kind of event through video analysis (TODO ask Catarina for a better definition).

There are multiple types of Analytics (AN), each responsible for observing and analyzing a different kind of event or behaviour, and identified by a unique numerical ID (e.g. AN1, AN2). As of this writing, the following Analytic types are accounted for:

. AN1 - vehicle passage counter. It detects that a vehicle has passed through a given road. (TODO what is the goal? To register these events in a database? If so, what database? Does it strictly count the number of vehicle passing through, or does it categorize them according to type such as "Car", "Truck", or other parameters? Is this a concern that I do not need to have?);

. AN2 - wrong-way driving (WWD, *contramão* in Portuguese). It detects that a vehicle is driving against the correct direction of traffic, which is an incredibly dangerous behavior;

. AN3 - stopped vehicle. It detects that a vehicle is stopped/parked in a motorway.

### Use Cases

#### . UC1 - Start Analytic

  The Video Server should be able to start an Analytic upon receiving an external HTTP request.

  The HTTP request for an Analytic will include an ":id" parameter in its route (e.g. "videoserver/api/analytic/1" for AN1).

  If the request is properly formed, the Video Server must **start** the corresponding Analytic, triggering its executable (a C++ process, in the current implementation). The HTTP response must produce a corresponding "200 OK" status code.

  If the request is not successful due to client-side errors, the Video Server must respond with an appropriate "4xx" status code (e.g. "404" for not found, "403" for unauthorized access, etc).

#### . UC2 - Stop Analytic

  The Video Server should be able to stop an Analytic upon receiving an external HTTP request.

  The HTTP request for an Analytic will include an ":id" parameter in its route (e.g. "videoserver/api/analytic/1" for AN1).

  If the request is properly formed, the Video Server must **stop** the corresponding Analytic, terminating its executable (a C++ process, in the current implementation). The HTTP response must produce a corresponding "200 OK" status code.

  If the request is not successful due to client-side errors, the Video Server must respond with an appropriate "4xx" status code (e.g. "404" for not found, "403" for unauthorized access, etc).



## Non-Functional Requirements (URPS+)

### Usability:

 The new API endpoints for Analytics must be clear and well-documented, regarding their purposes.

### Reliability:

 The introduction of the new Analytics features must not compromise the existing ones in the current version of the Video Server.

### Performance:

 The Video Server must be non-blocking. Special care must be taken to make full use of the full features made available by Vert.x to maximize asynchronous performance.

 As an example, Vert.x provides different types of Verticles (https://vertx.io/docs/vertx-core/java/#_verticle_types). Using the right one in the right situation will be crucial.

### Supportability:

 The system must allow adding different kinds of Analytics in the future

### Design requirements:

 New components/modules must comply with the current Video Server architecture. Using appropriate design patterns and principles (e.g. SOLID, GRASP) in order to achieve quality is a must.

### Implementation requirements:

  Video Server is already developed in Vert.x / Java;

### Interface requirements:

  External requests will come through in HTTP;

  Specify external HTTP route formats here (e.g. "videoserver/api/analytic/:id")

  Specify any necessary HTTP body constraints here (e.g. JSON structure in a POST request)

  HTTP responses must contain industry-standard status codes appropriate for each situation and, whenever applicable, the response body should contain any additional data that the client might find useful.

  The Video Server must interface with an Analytic external component, written in C++ (Catarina took care of this 100%, from what I can gather, by writing a prototypical, isolated Vert.x program that can call the C++ executable. I "just" have to integrate this proof-of-concept into the main Video Server architecture)


### Physical requirements:
